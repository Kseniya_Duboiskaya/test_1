# Task 1

Write a Java application (command-line) that prompts the user for a text string, performs a Web search
(Google/Yahoo/Bing/etc - your choice) and returns the title and URL of the first result. Use any tools /
libraries you wish, but you must provide instructions on how to build and run your application. Please
include a brief description of your application and why you implemented it the way you did.

# Instructions

## Settings:
1. Download and unzip the project.
2. Install node.
3. Go through the console to the root of the project. Run the command npm i.

## For start
1. Run the command npm start.
2. Enter string for search in Google.

# Motivation

I've used the google-search package because it provides a convenient interface for [Google Custom Search API] (https://developers.google.com/custom-search/json-api/v1/overview).
And I've used the prompt package because it's a beautiful command-line prompt for node.js.