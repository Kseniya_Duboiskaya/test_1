import GoogleSearch from 'google-search';

export default class Searcher {
    constructor() {
        this.googleSearch = new GoogleSearch({
            key: 'AIzaSyDgnPoIjSv9auLSzRkWyYG4uJ3xxU6afAw',
            cx: '011924301150680107002:lh6-339wx_o',
        });
    }


    find = (param, cb = () => {}) =>
        this.googleSearch.build({
        q: param,
        num: 1,
    }, cb)
}
