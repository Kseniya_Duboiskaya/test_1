import Promise from 'bluebird';
import SearchCreator from './SearchCreator';

const prompt = Promise.promisifyAll(require('prompt'));

function formAnswer(request) {
    return request.items ? `Title: ${request.items[0].title}  Link: ${request.items[0].link}` : 'Nothing found.';
}

function start() {
    const googleSearch = Promise.promisifyAll(SearchCreator.createSearch());
    prompt.start();
    prompt
        .getAsync([{
            name: 'request',
            required: true,
            message: 'Enter your request',
        }])
        .get('request')
        .then(googleSearch.findAsync)
        .then(formAnswer)
        .then(console.log)
        .catch(console.log);
}

start();


