import Searcher from './Searcher';

export default class SearchCreator {

    static createSearch () {
        return new Searcher();
    }
}